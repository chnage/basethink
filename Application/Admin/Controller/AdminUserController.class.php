<?php
/**
 * 后台用户信息管理控制器
 * Created by PhpStorm.
 * User: change
 * Date: 2016/12/22
 * Time: 22:00
 */
namespace Admin\Controller;

use Base\Controller\CurdController;
use Library\UploadFile;

/**
 * 用户管理
 * Class UserController
 * @package Admin\Controller
 */
class AdminUserController extends CurdController
{
    public function __construct()
    {
        parent::__construct(true, true, array(
            'input_name' => 'coverFile',//文本框name
            'save_path' => 'head',     //保存路径
            'data_field' => 'cover'    //数据库字段
        ));
    }

    /**
     * 获取所有数据并注入模板
     * */
    protected function getAll()
    {
        if(I('get.del')){
            $map['status'] = 0;
        }else{
            $map['status'] = 1;
        }
        if ($this->relation) {
            $list = $this->model->relation($this->relation)->where($map)->order('id desc')->select();
        }
        $this->assign('list', $list);
    }

    /**
     * 编辑用户
     */
    public function edit()
    {
        $data = $this->model->relation(true)->create();
        if (!$data) {
            $this->error($this->model->getError());
        } else {
            if ($this->upload) {
                $input_name = $this->uploadConfig['input_name'];
                $save_path = $this->uploadConfig['save_path'];
                $data_field = $this->uploadConfig['data_field'];
                if ($_FILES[$input_name]) {
                    $sel = $this->model->find($data['id']);
                    if ($sel[$data_field]) {
                        //删除
                        $upConfig = C('UPLOADS');
                        removeFile($upConfig['PATH'] . '/' . $sel[$data_field]);
                    }
                    $up = new UploadFile();
                    $info = $up->upFile($save_path, array('jpg', 'png', 'jpeg', 'gif'));
                    $data[$data_field] = $info[$input_name]['savepath'] . $info[$input_name]['savename'];
                }
            }
            $add = $this->model->save($data);
            if ($add) {
                $this->success('操作成功');
            }
        }
    }

    /**
     * 新增用户
     */
    public function add()
    {
        $data = $this->model->create();
        if(!$data){
            $this->error($this->model->getError());
        }else{
            if ($this->upload) {
                $input_name = $this->uploadConfig['input_name'];
                $save_path = $this->uploadConfig['save_path'];
                $data_field = $this->uploadConfig['data_field'];
                if ($_FILES[$input_name]) {
                    $up = new UploadFile();
                    $info = $up->upFile($save_path, array('jpg', 'png', 'jpeg', 'gif'));
                    $data[$data_field] = $info[$input_name]['savepath'] . $info[$input_name]['savename'];
                }
            }
            $add = $this->model->add($data);
            if ($add) {
                $this->success('新增成功', $this->urlConfig['add_success']);
            }
        }
    }
}