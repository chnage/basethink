<?php
/**
 * 管理后台控制器
 * Class ControlController
 * @package Admin\Controller
 */
namespace Admin\Controller;

use Base\Controller\BaseAdminController;
use Think\Controller;

class ControlController extends BaseAdminController
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * 后台框架
     */
    public function index()
    {
        if(!session('user')){
            //跳转登录页面
            $this->redirect('Index/index');
        }
        $this->assign('menuList', $this->getMenuList());
        $this->show('');
    }

    /**
     * 后台信息
     */
    public function msg(){

        $this->show('');
    }

    /**
     * 退出登录
     */
    public function logout()
    {
        session('user', null);
        $this->redirect('Index/index');
    }

    /**
     * 后台首页
     */
    public function welcome()
    {
        $this->show('');
    }

    /**
     * 更新缓存
     */
    public function updateCache()
    {
        removeDir(CACHE_PATH);
        $this->success('缓存更新成功', U('welcome'));
    }
}