<?php
namespace Admin\Controller;

use Base\Controller\CurdController;
use Library\UploadFile;


/**
 * 系统设置
 * Class ConfigController
 * @package Admin\Controller
 */
class ConfigController extends CurdController
{

    public function __construct()
    {
        parent::__construct(false);
    }

    /**
     * 站点设置
     */
    public function siteInfo()
    {
        //站点信息
        $this->assign('site_name', $this->getConfig('site_name'));
        $this->assign('site_keywords', $this->getConfig('site_keywords'));
        $this->assign('site_description', $this->getConfig('site_description'));
        $this->assign('service_tel', $this->getConfig('service_tel'));
        $this->assign('wechat', $this->getConfig('wechat'));
        $this->assign('site_copy_right', $this->getConfig('site_copy_right'));
        $this->assign('site_company', $this->getConfig('site_company'));
        $this->assign('site_icp', $this->getConfig('site_icp'));
        //上传配置
        $this->assign('upload_compress', $this->getConfig('upload_compress'));
        $this->assign('upload_size', $this->getConfig('upload_size'));
        $this->show('');
    }

    /**
     * 提交站点设置
     * @param string $act
     */
    public function submit($act)
    {
        switch ($act) {
            case 'siteInfo':
                $this->setConfig('site_name', I('post.site_name'));                //站点名称
                $this->setConfig('site_keywords', I('post.site_keywords'));        //网站关键词
                $this->setConfig('site_description', I('post.site_description'));  //网站描述
                $this->setConfig('service_tel',I('post.service_tel'));             //客服电话
                //上传二维码图片
                $input_name = $this->uploadConfig['input_name'];
                $save_path = $this->uploadConfig['save_path'];
                if ($_FILES[$input_name]) {
                    $old_cover = $this->getConfig('wechat');
                    $up = new UploadFile();
                    $info = $up->upFile($save_path, array('jpg', 'png', 'jpeg', 'gif'));
                    $cover_path = $info[$input_name]['savepath'] . $info[$input_name]['savename'];
                    $result = $this->setConfig('wechat',$cover_path);                       //官方微信
                    if ($result){
                        if ($old_cover) {
                            //删除
                            $upConfig = C('UPLOADS');
                            removeFile($upConfig['PATH'] . '/' . $old_cover);
                        }
                    }
                }
                $this->setConfig('site_copy_right', I('post.site_copy_right'));    //底部版权
                $this->setConfig('site_company', I('post.site_company'));          //公司信息
                $this->setConfig('site_icp', I('post.site_icp'));                  //备案号

                $this->setConfig('upload_compress', I('post.upload_compress') ? 1 : 0);   //上传图片压缩
                $this->setConfig('upload_size', I('post.upload_size'));                   //单个文件上传限制
                $this->success('操作成功');
                break;
        }
    }

}