<?php
/**
 * 菜单管理控制器
 * Created by PhpStorm.
 * User: change
 * Date: 2016/12/22
 * Time: 16:09
 */

namespace Admin\Controller;


use Base\Controller\CurdController;
use Library\Utils;

class MenuController extends CurdController
{
    public function __construct()
    {
        parent::__construct();
        $this->urlConfig = array(
            'add_success' => '',
            'add_error' => '',
            'edit_success' => '',
            'edit_error' => '',
            'del_success' => $_SERVER["HTTP_REFERER"],
            'del_error' => '',
        );
    }

    /**
     * 获取所有数据并注入模板
     * */
    protected function getAll()
    {
        if(I('get.del')){
            $map['status'] = 0;
        }else{
            $map['status'] = 1;
        }
        if (I('get.menu_id')) {
            $map['menu_id'] = I('get.menu_id');
        }
        if($_GET['keyword']){
            $map['title'] = array('like','%'.$_GET['keyword'].'%');
        }
        $list = $this->model->relation(false)->where($map)->select();

        if (I('get.menu_id') || I('get.keyword') || I('get.del')) {
            $list = Utils::toTree($list, '');
        }else{
            $list = Utils::toTree($list,'menu_id');
        }
        $this->assign('list', $list);
    }

}