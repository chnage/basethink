<?php
/**
 * 后台用户组管理
 * Class UserGroupController
 * @package Admin\Controller
 */
namespace Admin\Controller;

use Base\Controller\CurdController;

class UserGroupController extends CurdController
{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * 编辑界面
     * @param $id
     */
    public function showEdit($id)
    {
        if (!$id) {
            $this->error('非法操作');
        }
        $select = $this->model->find($id);
        if (empty($select)) {
            $this->error('非法操作');
        }
        $this->assign('data', $select);

        $this->show('');
    }

    public function editAfter($data){
        return $data;
    }
}