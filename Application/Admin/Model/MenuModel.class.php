<?php
/**
 * Created by PhpStorm.
 * User: change
 * Date: 2016/12/21
 * Time: 18:22
 * 后台菜单
 */

namespace Admin\Model;


use Base\Model\BaseModel;

class MenuModel extends BaseModel
{
    protected $_validate = array(
        array('title', 'require', '名称必须填写'),
    );

    protected $_auto = array(
        array('status', '1'),
        array('create_time', 'time', self::MODEL_INSERT, 'function'),
        array('last_time', 'time', self::MODEL_BOTH, 'function'),
    );
    protected $_link = array(
        //包含子菜单
        'menu' => array(
            'mapping_type' => self::HAS_MANY,
            'foreign_key' => 'id',
            'parent_key' => 'menu_id',
            'mapping_order' => 'sort',
            'condition' => 'status=1',
        ),
    );
}