<?php
/**
 * Created by PhpStorm.
 * User: change
 * Date: 2016/12/21
 * Time: 20:28
 * 后台用户组
 */

namespace Admin\Model;


use Base\Model\BaseModel;

class UserGroupModel extends BaseModel
{
    protected $_validate = array(
        array('title', 'require', '名称必须填写', 1, '', 1),
    );

    protected $_auto = array(
        array('status', '1'),
        array('create_time', 'time', self::MODEL_INSERT, 'function'),
        array('last_time', 'time', self::MODEL_BOTH, 'function'),
        array('menu', 'json_encode', self::MODEL_BOTH, 'function'),
    );
}