<?php
/**
 * Created by PhpStorm.
 * User: change
 * Date: 2016/5/3
 * Time: 15:34
 */

namespace Library;


use Think\Controller;

/**
 * CC视频接口api通知
 * Class CcUtils
 * @package Library
 */
class CcUtils
{

    private $url = 'http://spark.bokecc.com/api/';
    private $cc_config;

    function __construct()
    {
        $this->cc_config = C('CC_CONFIG');
    }

    /**
     * 发送请求
     * @param $url
     * @param array $params
     * @return array
     */
    function http($url, $params = array())
    {
        $params['userid'] = $this->cc_config['userid'];
        $params['format'] = $this->cc_config['format'];
        ksort($params);
        $params['time'] = time();
        $params['salt'] = $this->cc_config['appkey'];
        $params['hash'] = md5(http_build_query($params));
        unset($params['salt']);
        return HttpUtils::http($this->url . $url, $params);
    }

    /**
     * 整理参数url
     * @param $params
     * @return string
     */
    function getUrl($params)
    {
        $params['userid'] = $this->cc_config['userid'];
//        $params['format'] = $this->cc_config['format'];
        ksort($params);
        $params['time'] = time();
        $params['salt'] = $this->cc_config['appkey'];
        $params['hash'] = md5(http_build_query($params));
        unset($params['salt']);
        $url = http_build_query($params);
        return $url;
    }

    /**
     * 获取账户基本信息
     * @return mixed
     */
    public function user()
    {
        $text = $this->http('user');
        $res = json_decode($text);
        return $res;
    }

    /**
     * 创建视频分类
     * @param string $title 分类名称
     * @param string $super_id 父分类id
     * @return mixed
     */
    public function createCategory($title = '', $super_id = '')
    {
        if (!$title) {
            return false;
        }
        if (!$super_id) {
            $super_id = $this->cc_config['category'];
        }
        $params = array(
            'name' => $title,
            'super_categoryid' => $super_id,
        );
        $text = $this->http('category/create', $params);
        $res = json_decode($text, true);
        return $res;
    }

    /**
     * 修改视频分类
     * @param string $categoryid 分类id
     * @param string $title 分类名称
     * @return mixed
     */
    public function categoryUpdate($categoryid = '', $title = '')
    {
        if (!$categoryid || !$title) {
            return false;
        }
        $params = array(
            'categoryid' => $categoryid,
            'name' => $title,
        );
        $text = $this->http('category/update', $params);
        $res = json_decode($text, true);
        return $res;
    }

    /**
     * 删除视频分类（真删除）
     * @param string $categoryid 分类id
     * @return mixed
     */
    public function categoryDelete($categoryid = '')
    {
        if (!$categoryid) {
            return false;
        }
        $params = array(
            'categoryid' => $categoryid,
        );
        $text = $this->http('category/delete', $params);
        $res = json_decode($text, true);
        return $res;
    }

    /**
     * 获取视频分类信息
     * @return mixed
     */
    public function videoCategory()
    {
        $text = $this->http('video/category');
        $res = json_decode($text, true);
        return $res;
    }
}