<?php

namespace Library;

use Think\Controller;
use Think\Image;
use Think\Upload;

/**
 * Class UploadFile
 * @package Library
 */
class UploadFile extends Controller
{

    public function __construct()
    {
        parent::__construct();
        
    }

    public function upFile($savePath = '', $exts = array(), $upSize = '')
    {
        $upSetting = C('UPLOADS');

        $config = array(
            'maxSize' => $upSetting['SIZE'],
            'rootPath' => $upSetting['PATH'],
            'savePath' => $savePath . '/',
            'saveName' => array('uniqid', ''),
            'exts' => $upSetting['EXT'],
            'autoSub' => true,
            'subName' => array('date', 'Ymd'),
        );
        if ($upSize > 0) $config['maxSize'] = $upSize * 1024 * 1024;
        if (!empty($exts)) {
            $config['exts'] = $exts;
        }

        $upload = new Upload($config);
        $info = $upload->upload();

        //$this->ajaxReturn(array('312'=>$upload->getError()),'json');
        if (!$info) {
            //return array('123'=>'312');
            //$this->error($upload->getError(), $ajax);
        } else {
            switch ($savePath) {
                case 'head':
                    break;
                default:
                    break;
            }
            return $info;
        }
    }

    /**
     * @param string $savePath
     * @param array $exts
     * @param string $upSize
     * @return array|bool
     */
    public function upDifferentImg($savePath = '', $exts = array(), $upSize = ''){
        //获取所有上传的图片
//        $str = "";
        $infos = array();
        foreach ($_FILES as $key=>$value){
            //获取每个上传图片的name
//            $name = $key;
//            $str .= "name名：$key";
//            foreach ($value as $k=>$v){
//                $str .= "{key:$k,value：$v}";
//            }
//            $str .= "<br>";
            //上传图片
            $info = $this->upOneFile($savePath);
            $infos[$key] = $info;
        }
//        $this->error($str);
//        return;

        return $infos;
    }

    function upOneFile($savePath = '', $exts = array(), $upSize = ''){
        $upSetting = C('UPLOADS');
        $saveName = $this->randno();
        $config = array(
            'maxSize' => $upSetting['SIZE'],
            'rootPath' => $upSetting['PATH'],
            'savePath' => $savePath . '/',
            'saveName' => $saveName,
            'exts' => $upSetting['EXT'],
            'autoSub' => true,
            'subName' => array('date', 'Ymd'),
            'mimes'      =>    array(),//允许上传的文件类型（留空为不限制），使用数组或者逗号分隔的字符串设置
             'saveExt'    =>    'jpg',//上传文件的保存后缀，不设置的话使用原文件后缀
        );
        if ($upSize > 0) $config['maxSize'] = $upSize * 1024 * 1024;
        if (!empty($exts)) {
            $config['exts'] = $exts;
        }
        $upload = new Upload($config);
        $info = $upload->upload();

        //$this->ajaxReturn(array('312'=>$upload->getError()),'json');
        if (!$info) {
            //return array('123'=>'312');
            //$this->error($upload->getError(), $ajax);
        } else {
            switch ($savePath) {
                case 'head':
                    break;
                default:
                    break;
            }
            return $info;
        }
    }

    /**
     * @return string
     * 生成随机时间
     */
    public function randno() {
        list($usec, $sec) = explode(" ", microtime());
        $usec = substr(str_replace('0.', '', $usec), 0 ,4);
        $str  = rand(10,99);
        return date("His").$usec.$str;
    }

    public function upload($savePath = '', $exts = array(), $upSize = ''){
        $upload = new Upload();// 实例化上传类
        $upload->maxSize   =     $upSize ;// 设置附件上传大小
        $upload->exts      =     array('jpg', 'gif', 'png', 'jpeg');// 设置附件上传类型
        $upload->rootPath  =     './Uploads/'; // 设置附件上传根目录
        $upload->savePath  =     ''; // 设置附件上传（子）目录
        // 上传文件
        $info   =   $upload->upload();
        if(!$info) {// 上传错误提示错误信息
            $this->error($upload->getError());
        }else{// 上传成功
//            $this->success('上传成功！');
            $path=$info['savepath'].$info['savename'];

            return $info;
        }
    }
}
