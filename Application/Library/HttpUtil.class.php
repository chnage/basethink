<?php

namespace Library;

use Exception;

/**
 * Created by PhpStorm.
 * User: Aistt
 * Date: 2016/4/8
 * Time: 9:58
 */
class HttpUtil
{

    /**
     * ����HTTP���󷽷���Ŀǰֻ֧��CURL��������
     * @param  string $url ����URL
     * @param  array $params �������
     * @param  string $method ���󷽷�GET/POST
     * @param array $header
     * @param bool $multi
     * @return array $data   ��Ӧ����
     * @throws Exception
     */
    static function http($url, $params = array(), $method = 'GET', $header = array(), $multi = false)
    {
        $opts = array(CURLOPT_TIMEOUT => 30, CURLOPT_RETURNTRANSFER => 1, CURLOPT_SSL_VERIFYPEER => false, CURLOPT_SSL_VERIFYHOST => false, CURLOPT_HTTPHEADER => $header);

        /* �����������������ض����� */
        switch (strtoupper($method)) {
            case 'POST' ://post
                //�ж��Ƿ����ļ�
                $params = $multi ? $params : http_build_query($params);
                $opts[CURLOPT_URL] = $url;
//                dump($opts[CURLOPT_URL]);
                $opts[CURLOPT_POST] = 1;
                $opts[CURLOPT_POSTFIELDS] = $params;
                break;
            default ://get
                $opts[CURLOPT_URL] = $url . '?' . http_build_query($params);
//                dump($opts[CURLOPT_URL]);
//                exit();
                break;
        }

        /* ��ʼ����ִ��curl���� */
        $ch = curl_init();
        curl_setopt_array($ch, $opts);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);//���������������ת
        $data = curl_exec($ch);
        $error = curl_error($ch);
        curl_close($ch);
        if ($error)
            throw new Exception('����������' . $error);
        return $data;
    }

    /**
     * ����Զ��ͼƬ������
     * @param $url
     * @param $path
     */
    static function saveImg($url, $path)
    {
        $img = file_get_contents($url);
        file_put_contents($path, $img);
        return $path;
    }
}