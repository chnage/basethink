<?php
namespace Library;

use Think\Controller;

/**
 * Created by PhpStorm.
 * User: change
 * Date: 2016/5/2
 * Time: 14:40
 */
class ColumnUtils extends Controller
{
    static public $treeList = array(); //存放无限分类结果如果一页面有多个无限分类可以使用 Tree::$treeList = array(); 清空

    /**
     * 无限级分类
     * @param $list
     * @param int $field 关联id字段名
     * @param int $column_id
     * @param int $level
     * @return array
     */
    static public function toTree($list, $field, $column_id = 0, $level = 0)
    {
        $column_id = (int)$column_id;
        foreach ($list as $key => $value) {
            if ($value[$field] == $column_id) {
                //对标题进行格式化
                $value['title'] = str_repeat('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;', $level) . '├─ ' . $value['title'];
                self::$treeList [] = $value;
//                unset($list[$key]);
                self::toTree($list, $field, $value['id'], $value['level'] + 1);
            }
        }
        return self::$treeList;
    }

    static public function clear()
    {
        self::$treeList = array();
    }
}