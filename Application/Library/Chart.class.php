<?php
/**
 * Created by PhpStorm.
 * User: change
 * Date: 2017/1/16
 * Time: 9:14
 * 环信聊天类
 */

namespace Library;


class Chart
{
    private $options;     //环信参数
    private $easeMob;     //环信类对象

    public function __construct()
    {
        $this->options = array(
            'client_id' => '',   //填写id
            'client_secret' => '-wc',  //填写secret
            'org_name' => '',    //企业的唯一标识，开发者在环信开发者管理后台注册账号时填写的企业 ID
            'app_name' => ''          //同一“企业”下“APP”唯一标识，开发者在环信开发者管理后台创建应用时填写的“应用名称”
        );
        //生成环信类对象
        $this->easeMob = new Easemob($this->options);
    }

    /**
     * 获取token
     */
    public function getToken(){
        //获取token
        $token = $this->easeMob->getToken();
        return $token;
    }

    /**
     * @param $name
     * @param $pwd
     * 注册环信用户
     */
    public function registerUser($name,$pwd){
        $this->easeMob->createUser($name,$pwd);
    }

    /**
     * @param $name
     * @param $friend
     * @return mixed
     * 环信添加好友
     */
    public function addFriend($name,$friend){
        return $this->easeMob->addFriend($name,$friend);
    }

    /**
     * @param $name
     * @return mixed
     * 获取好友列表
     */
    public function getFriendList($name){
        return $this->easeMob->showFriends($name);
    }
}