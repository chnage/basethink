<?php
/**
 * Created by PhpStorm.
 * User: change
 * Date: 2016/5/23
 * Time: 14:41
 */

function get_extension($file)
{
    return substr(strrchr($file, '.'), 1);
}

/**
 * 删除文件夹
 * @param $dir
 */
function removeDir($dir)
{
    $dh = opendir($dir);
    while ($file = readdir($dh)) {
        if ($file != "." && $file != "..") {
            $fullpath = $dir . "/" . $file;
            if (!is_dir($fullpath)) {
                unlink($fullpath);
            } else {
                removeDir($fullpath);
            }
        }
    }
}

/**
 * 删除文件
 * @param string $path 文件路径
 * @return bool
 */
function removeFile($path)
{
    if (!file_exists($path)) {
        return true;
    }
    if (unlink($path)) {
        return true;
    } else {
        return false;
    }
}
/**
 * 获取富文本中的全部图片路径
 * @param string $pageContents
 * @return array
 */
function getContentImgs($pageContents){
    $pageContents = str_replace('\"','"',$pageContents);
    $reg = '/<img (.*?)+src=[\'"](.*?)[\'"]/i';
    preg_match_all($reg, htmlspecialchars_decode($pageContents), $results);
//    $results[2]

//    $result = preg_replace ( "/.*<img[^>]*src[=\s\"\']+([^\"\']*)[\"\'].*/", "$1", $img );

//    preg_match('/<img.+src=\"?(.+\.(jpg|jpeg|gif|bmp|bnp|png))\"?.+>/i',$pageContents,$match);

//    $pageContents="<html><div><img src='dadsa.png'></div></html>";

//    preg_match('/<\s*img\s+[^>]*?src\s*=\s*(\'|\")(.*?)\\1[^>]*?\/?\s*>/i',htmlspecialchars_decode($pageContents),$match);

    return $results[2];
}
/**
 * 获取富文本中的全部文件路径
 * @param string $pageContents
 * @return array
 */
function removeContentFiles($pageContents){
    //删除图片
    $pageContents = str_replace('\"','"',$pageContents);
    $reg = '/<img (.*?)+src=[\'"](.*?)[\'"]/i';
    preg_match_all($reg, htmlspecialchars_decode($pageContents), $imgs);
    if (!empty($imgs[2]) && count($imgs[2])>0){
        foreach ($imgs[2] as $pkey=>$pvlue){
            removeFile(dirname(THINK_PATH).$pvlue);
        }
    }
    //删除文件
    $reg = '/<a (.*?)+href=[\'"](.*?)[\'"]/i';
    preg_match_all($reg, htmlspecialchars_decode($pageContents), $files);
    if (!empty($files[2]) && count($files[2])>0) {
        foreach ($files[2] as $fkey=>$fvlue){
            removeFile(dirname(THINK_PATH).$fvlue);
        }
    }
}

/**
 * 生成唯一文件名
 * @return string
 */
function getFileName()
{
    return microtime(true) * 10000;
}

/**
 * 获取文件夹大小
 * @param $dir
 * @return int
 */
function getDirSize($dir)
{
    $handle = opendir($dir);
    $sizeResult = 0;
    while (false !== ($FolderOrFile = readdir($handle))) {
        if ($FolderOrFile != "." && $FolderOrFile != "..") {
            if (is_dir("$dir/$FolderOrFile")) {
                $sizeResult += getDirSize("$dir/$FolderOrFile");
            } else {
                $sizeResult += filesize("$dir/$FolderOrFile");
            }
        }
    }
    closedir($handle);
    return $sizeResult;
}

/**
 * 单位自动转换函数
 * @param $size
 * @return string
 */
function getRealSize($size)
{
    $kb = 1024;
    $mb = 1024 * $kb;
    $gb = 1024 * $mb;
    $tb = 1024 * $gb;
    if ($size < $kb) {
        return $size . " B";
    } else if ($size < $mb) {
        return round($size / $kb, 2) . " KB";
    } else if ($size < $gb) {
        return round($size / $mb, 2) . " MB";
    } else if ($size < $tb) {
        return round($size / $gb, 2) . " GB";
    } else {
        return round($size / $tb, 2) . " TB";
    }
}