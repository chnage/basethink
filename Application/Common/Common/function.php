<?php

/**
 * Created by PhpStorm.
 * User: Aistt
 * Date: 2016/3/4
 * Time: 11:39
 */
use Admin\Model\MenuModel;
use Library\ColumnUtils;
use Base\Controller\BaseAdminController;


/**
 * @param $table
 * @param $id
 * @param $field
 * @return mixed 获取一条数据
 * 获取一条数据
 */
function getTableRowOrField($table, $id, $field)
{
    if (!$id) return null;
    $sel = M($table)->where('status=1 and id=' . $id)->find();
    return $field ? $sel[$field] : $sel;
}

/**
 * 判断是否父栏目
 * @param int $id 子栏目
 * @param int $id2 父栏目
 * @return bool
 */
function isPro($id, $id2)
{
    if ($id == $id2) {
        return true;
    }
    $model = M('column');
    $sel1 = $model->find($id);
    $sel2 = $model->find($id2);
    if (empty($sel1) || empty($sel2)) {
        return false;
    }
    if ($sel1['column_id'] == $id2) {
        return true;
    } else {
        return isPro($sel1['column_id'], $id2);
    }
}

/**
 * 判断是否父目录
 * @param int $id 子栏目
 * @param int $id2 父栏目
 * @return bool
 */
function isProCatalog($id, $id2)
{
    if ($id == $id2) {
        return true;
    }
    $model = M('catalog');
    $sel1 = $model->find($id);
    $sel2 = $model->find($id2);
    if (empty($sel1) || empty($sel2)) {
        return false;
    }
    if ($sel1['catalog_id'] == $id2) {
        return true;
    } else {
        return isProCatalog($sel1['catalog_id'], $id2);
    }
}

//判断是否父栏目
function isParentCol($id){
    $model = M('column');
    $map['column_id']=$id;
    $map['status']=1;
    $data=$model->where($map)->select();
    if (!$data){
        return true;
    }else{
        return false;
    }
}
//查询栏目名称
function getColumnName($id){
    if($id){
        $map['status'] = 1;
        $map['id'] = $id;
        $data=M('column')->where($map)->find();
        return $data['title'];
    }
}

//查询栏目类型
function getColumnModel($id)
{
    if($id){
        $data = M('column')->where('id=' . $id)->find();
        return $data['model_id'];
    }
}

function isProMenu($id, $id2)
{
    if ($id == $id2) {
        return true;
    }
    $model = M('menu');
    $sel1 = $model->find($id);
    $sel2 = $model->find($id2);
    if (empty($sel1) || empty($sel2)) {
        return false;
    }
    if ($sel1['menu_id'] == $id2) {
        return true;
    } else {
        return isProMenu($sel1['menu_id'], $id2);
    }
}

//获取子栏目列表
function getSubColumn($columnId,$limit){
    $model = M('column');
    $data = $model->where(array('status' => 1, 'column_id' => $columnId))->limit($limit)->select();
    
    return $data;
}

/**
 * 读取数据列表
 * @param string $tableName 表名称
 * @param string $map
 * @param bool $relation
 * @param string $order
 * @param string $limit
 * @return array 返回数据集合
 */
function getList($tableName, $map = 'status = 1', $relation = false, $order = 'id desc', $limit = '')
{
    if ($relation) {
        if ($limit) {
            $select = D($tableName)->relation(true)->where($map)->order($order)->limit($limit)->select();
        } else {
            $select = D($tableName)->relation(true)->where($map)->order($order)->select();
        }
    } else {
        if ($limit) {
            $select = M($tableName)->where($map)->order($order)->limit($limit)->select();
        } else {
            $select = M($tableName)->where($map)->order($order)->select();
        }
    }
    return $select;
}

/**
 * 菜单树状图
 * @param int $id
 * @param int $level
 * @return array|mixed
 */
function getMenuTree($id = 0, $level = 0)
{
    ColumnUtils::clear();
    $menuModel = new MenuModel();
    $list = $menuModel->relation(true)->where('status=1')->select();
//    if ($id) {
//        foreach ($list as $key => $value) {
//            if (!isProMenu($value['id'], $id)){
//                unset($list[$key]);
//            } else{
//                $list[$key]['sa'] = $value['id'];
//            }
//        }
//    }
    $list = ColumnUtils::toTree($list, 'menu_id', $id, $level);
    return $list;
}

/**
 * 获取配置
 * @param $key
 */
function getConfig($key)
{
    $map = array(
        'key' => $key,
    );
    $c = M('config')->where($map)->find();
    return $c['value'];
}

/**
 * 截取utf-8字符串
 * @since 2008.12.23
 * @param string $str 被截取的字符串
 * @param integer $start 起始位置
 * @param integer $length 截取长度(每个汉字为3字节)
 * @return string
 */
function utf8_strcut($str, $start, $length = null)
{
    $str = strip_tags(htmlspecialchars_decode($str));
    $str = str_replace('&nbsp;', '', $str);
    preg_match_all('/./us', $str, $match);
    $chars = is_null($length) ? array_slice($match[0], $start) : array_slice($match[0], $start, $length);

    unset($str);

    return implode('', $chars);
}

/**
 * 判断是否手机访问
 * @return bool
 */
function isMobile()
{
    // 如果有HTTP_X_WAP_PROFILE则一定是移动设备
    if (isset ($_SERVER['HTTP_X_WAP_PROFILE'])) {
        return true;
    }
    // 如果via信息含有wap则一定是移动设备,部分服务商会屏蔽该信息
    if (isset ($_SERVER['HTTP_VIA'])) {
        // 找不到为flase,否则为true
        return stristr($_SERVER['HTTP_VIA'], "wap") ? true : false;
    }
    // 脑残法，判断手机发送的客户端标志,兼容性有待提高
    if (isset ($_SERVER['HTTP_USER_AGENT'])) {
        $clientkeywords = array(
            'nokia', 'sony', 'ericsson', 'mot', 'samsung', 'htc', 'sgh', 'lg', 'sharp',
            'sie-', 'philips', 'panasonic', 'alcatel', 'lenovo', 'iphone', 'ipod', 'blackberry',
            'meizu', 'android', 'netfront', 'symbian', 'ucweb', 'windowsce', 'palm',
            'operamini', 'operamobi', 'openwave', 'nexusone', 'cldc', 'midp', 'wap', 'mobile'
        );
        // 从HTTP_USER_AGENT中查找手机浏览器的关键字
        if (preg_match("/(" . implode('|', $clientkeywords) . ")/i", strtolower($_SERVER['HTTP_USER_AGENT']))) {
            return true;
        }
    }
    // 协议法，因为有可能不准确，放到最后判断
    if (isset ($_SERVER['HTTP_ACCEPT'])) {
        // 如果只支持wml并且不支持html那一定是移动设备
        // 如果支持wml和html但是wml在html之前则是移动设备
        if ((strpos($_SERVER['HTTP_ACCEPT'], 'vnd.wap.wml') !== false) && (strpos($_SERVER['HTTP_ACCEPT'], 'text/html') === false || (strpos($_SERVER['HTTP_ACCEPT'], 'vnd.wap.wml') < strpos($_SERVER['HTTP_ACCEPT'], 'text/html')))) {
            return true;
        }
    }
    return false;
}

//获取用户信息
function getUserInfo(){
    $base = new BaseAdminController();
    $user=$base->getUserInfo();
    return $user;
}
