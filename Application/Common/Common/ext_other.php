<?php
/**
 * Created by PhpStorm.
 * User: change
 * Date: 2016/5/23
 * Time: 14:41
 */

/**
 * 驼峰转下划线
 * @param $str
 * @return string
 */
function hump2underline($str)
{
    return strtolower(preg_replace('/((?<=[a-z])(?=[A-Z]))/', '_', $str));
}

/**
 * 下划线转驼峰
 * @param $str
 * @param bool|true $ucfirst 首字母大写
 * @return mixed|string
 */
function underline2hump($str , $ucfirst = true)
{
    $str = ucwords(str_replace('_', ' ', $str));
    $str = str_replace(' ','',lcfirst($str));
    return $ucfirst ? ucfirst($str) : $str;
}