<?php
/**
 * Created by PhpStorm.
 * User: change
 * Date: 2016/12/14
 * Time: 12:30
 */

namespace Common\Tag;

use Think\Template\TagLib;

/**
 * Class TagLibTag
 * @package Common\Tag
 * 前台模板标签库
 */
class TagLibTag extends TagLib
{

    protected $tags = array(
        'ad' => array(
            'attr' => 'id',
            'close' => 0,
        )
    );

    /**
     * 广告调用标签
     * @param $attr
     * @return string
     */
    public function _ad($attr)
    {
        $id = $attr['id'];
        $data = M('Advert')->where('status=1 and id=%d', array($id))->find();
        $str = '';
        if ($data['open']) {
            $data['link'] = $data['link'] ? $data['link'] : 'javascript:void(0);';
            $str .= '<a href="' . $data['link'] . '" target="_blank"><img class="ad" src="__UPS__' . $data['cover'] . '" width="' . $data['width'] . '" height="' . $data['height'] . '" /></a>';
        }
        return $str;
    }

}

