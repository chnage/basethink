<?php
return array(
    //'配置项'=>'配置值'
    'DB_TYPE' => 'mysql',// 数据库类型
    'DB_HOST' => '127.0.0.1',// 服务器地址
    'DB_NAME' => 'basethink',// 数据库名
    'DB_USER' => 'root',// 用户名
    'DB_PWD' => 'root',// 密码
    'DB_PORT' => '',// 端口
    'DB_PREFIX' => 'b_',// 数据库表前缀

    'URL_MODEL' => 2,
    'TMPL_FILE_DEPR' => '_',

    'DATA_CACHE_TIME' => 60,

    'DEFAULT_THEME' => 'Html',//默认主题

    'URL_CASE_INSENSITIVE'  =>  false,     //严格的url大小写
    'TMPL_ACTION_ERROR' => 'Common_jump',    //成功提示页面
    'TMPL_ACTION_SUCCESS' => 'Common_jump',  //失败提示页面

    //模板调用
    'TMPL_PARSE_STRING' => array(
        '__UPS__' => __ROOT__ . '/Uploads/',
        '__HUI__' => __ROOT__ . '/Public/Framework/hui/',
        '__MUI__' => __ROOT__ . '/Public/Framework/mui/',
        '__BOOT__' => __ROOT__ . '/Public/Framework/bootstrap/',
        '__AJS__' => __ROOT__ . '/Template/Admin/Js/',
        '__ACSS__' => __ROOT__ . '/Template/Admin/Css/',
        '__AIMG__' => __ROOT__ . '/Template/Admin/Img/',

        '__APICSS__' => __ROOT__ . '/Template/Api/Css/',
        '__PUBSTA__' => __ROOT__ . '/Public/Static/',

        '__HIMG__' => __ROOT__ . '/Template/Home/Img/',
        '__HCSS__' => __ROOT__ . '/Template/Home/Css/',
        '__HJS__' => __ROOT__ . '/Template/Home/Js/',

        '__APIIMG__' => __ROOT__ . '/Template/Api/Img/',
        '__APICSS__' => __ROOT__ . '/Template/Api/Css/',
        '__APIJS__' => __ROOT__ . '/Template/Api/Js/'
    ),
    //上传路径
    'UPLOADS' => array(
        'PATH' => dirname(THINK_PATH) . '/Uploads/',
        'EXT' => array('jpg', 'png', 'gif', 'jpeg'),
        'SIZE' => 1024 * 1024 * 100,
        'DIR' => './Uploads/',
    ),
//    开启静态缓存
//    'HTML_CACHE_ON'     =>    true, // 开启静态缓存
//    'HTML_CACHE_TIME'   =>    180,   // 全局静态缓存有效期（秒）
//    'HTML_FILE_SUFFIX'  =>    '.html', // 设置静态缓存文件后缀
//    'HTML_CACHE_RULES'  =>     array(  // 定义静态缓存规则
//        // 定义格式 数组方式
//        'read'=>array('{id}',180)
//    ),

    //拓展类库
    'AUTOLOAD_NAMESPACE' => array(
        'Lib' => APP_PATH . 'Library',
    ),

    //无需权限验证的控制器
    'NO_AUTH_CONTROLLER' => array(
        'admin' => array(),
    ),

    //模板标签
    'TAGLIB_LOAD' => true,
    'TAGLIB_BUILD_IN' => 'Cx,Common\Tag\TagLibTag',

    //拓展函数库
    'LOAD_EXT_FILE' => 'ext_file,ext_other'
);