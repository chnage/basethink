<?php
namespace Base\Controller;

use Library\UploadFile;
use Think\Controller\RestController;

class BaseApiController extends RestController
{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param string $data
     * 成功返回
     * @param string $msg
     */
    public function apiSuccess($data, $msg = '')
    {
        $this->response(array(
            'data' => $data,
            'msg' => $msg,
            'code' => 1,
        ),'json');
        exit();
    }

    /**
     * @param string $msg
     * @param int $code
     * 错误返回
     */
    public function apiError($msg = '',$code = 0)
    {
        $this->response(array(
            'data' => null,
            'msg' => $msg,
            'code' => $code,
        ),'json');
        exit();
    }

    /**
     * @param bool $is_msg
     * @param int $user_id
     * 检测用户token是否有效
     */
    public function checkUserToken($is_msg = true,$user_id = 0){
        if (empty($user_id)){
            $user_id = $_POST['user_id'];
        }
        if(empty($user_id)||empty($_POST['token'])){
            $this->apiError('用户id或者token不能为空！');
        }
        $data = M('user')->where(array('id'=>$user_id,'token'=>$_POST['token'],'status'=>1))->getField('id');
        if (empty($data)){
            session('user_id',null);  //删除session
            $this->apiError('token已失效',-1);
        }else{
            if ($is_msg){
                session('user_id',$data); //保存session
                $this->apiSuccess($data,'token有效');
            }
        }
    }

    /**
     * @param string $inputname
     * @return bool|string
     * 上传图片
     */
    protected function upImgFile($inputname='cover'){
        if ($_FILES[$inputname]){
            //上传图片
            $input_name = 'cover';
            $up = new UploadFile();
            $info = $up->upFile($input_name, array('jpg', 'png', 'jpeg'));
            if (empty($info)){
                return false;
            }else{
                $cover = $info[$inputname]['savepath'] . $info[$inputname]['savename'];
                return $cover;
            }
        }
    }

    /**
     * @param string $name
     * @return bool|string
     * 上传视频
     */
    protected function upVideoFile($name = 'video'){
        //上传视频
        if ($_FILES[$name]) {
            $up = new UploadFile();
            $info = $up->upFile('video', array('mp4', 'avi', 'wmv', 'rm', 'rmvb', '3gp'));
            if (empty($info)){
                return false;
            }else{
                return $info[$name]['savepath'] . $info[$name]['savename'];
            }
        }
    }

    /**
     * @param string $pwd
     * 验证密码
     */
    protected function checkPwd($pwd='pwd'){
        //判断密码是否纯数字
        if(is_numeric($_POST[$pwd]) and strlen($_POST[$pwd])<8){
            $this->apiError('纯数字密码最少8位！');
        }
        if(strlen($_POST[$pwd])<6){
            $this->apiError('密码最少6位！');
        }
    }

    /**
     * @param string $input_name
     * @return mixed
     * 上传多张图片
     */
    public function upMoreImg($input_name='picture'){
        //上传多张图片
        $covers = $_FILES[$input_name];
        $type = false;
        if ($covers){
            //获取所有图片路径
            $names = $covers['name'];
        }else{
            $type = true;
            $covers = $_FILES[$input_name.'1'];
            for ($i=1;$i<=6;$i++){
                $inputName = $input_name.$i;
                $pFile = $_FILES[$inputName];
                if ($pFile){
                    $names[] = $pFile['name'];
                }
            }
        }
        if ($covers){
            $up = new UploadFile();
            $add = 0;
            if ($type){
                $info = $up->upDifferentImg($input_name);
            }
            foreach ($names as $k=>$v){
                if ($type){
                    $inpName = $input_name.($k+1);
                    $picture = $info[$inpName][$inpName];
                }else{
                    $info = $up->upDifferentImg($input_name);
                    $picture = $info[$input_name][$k];
                }
                $paths[] = $picture['savepath'].$picture['savename'];
            }
        }
        return $paths;
    }

    /**
     * @param $array
     * @return array
     * 去除数组重复元素
     */
    function a_array_unique($array)//写的比较好
    {
        $out = array();
        foreach ($array as $key=>$value) {
            if (!in_array($value, $out))
            {
                $out[$key] = $value;
            }
        }
        return $out;
    }


}