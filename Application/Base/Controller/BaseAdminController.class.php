<?php
namespace Base\Controller;

use Admin\Model\MenuModel;
use Admin\Model\UserGroupModel;
use Admin\Model\UserModel;
use Think\Controller;

class BaseAdminController extends Controller
{
    protected $user;
    protected $userGroup;

    public function __construct()
    {
        parent::__construct();

        $user = $this->getUser();
        if (!empty($user)) {
            $this->user = $user;
            $this->assign('user', $user);
        }
    }

    //验证密码
    protected function checkPwd(){
        //判断密码是否纯数字
        if(is_numeric($_POST['pwd']) and strlen($_POST['pwd'])<8){
            $this->error('纯数字密码最少8位！');
        }
    }

    /**
     * @return bool
     * 当前是否登录
     */
    protected function isLogin()
    {
        return $this->getUser() ? true : false;
    }

    /**
     * 判断是否登录
     */
    protected function getUser()
    {
        return session('user');
    }

    /**
     * @return mixed
     * 获取菜单
     */
    protected function getMenuList()
    {
        $userGroupModel = new UserGroupModel();
        $userGroup = $userGroupModel->getOne($this->user['user_group_id']);
        $menu = json_decode($userGroup['menu']);
        $model = new MenuModel();
        $data = $model->relation(true)->where('status=1 and menu_id < 1 or menu_id is null')->select();
        foreach ($data as $k => $v) {
            if (!in_array($v['id'], $menu)) unset($data[$k]);
            foreach ($v['menu'] as $kk => $vv) {
                if (!in_array($vv['id'], $menu)) unset($data[$k]['menu'][$kk]);
            }
        }
        return $data;
    }

    //查询用户信息
    public function getUserInfo(){
        $userModel = new UserModel();
        $user = $userModel->getOne($this->user['id']);
        return $user;
    }

}