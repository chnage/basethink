<?php
/**
 * Created by PhpStorm.
 * User: Jacky
 * Date: 2015/11/16
 * Time: 14:06
 */

namespace Base\Model;

use Think\Model\RelationModel;
use Think\Page;

class BaseModel extends RelationModel
{

    /**
     * @param $id
     * 获取一个
     * @param bool $relation
     * @return mixed
     */
    public function getOne($id, $relation = false)
    {
        return $this->relation($relation)->where('status=1 and id=%d', $id)->find();
    }

    /**
     * @param string $where  条件
     * @param bool $relation 关联
     * @param string $order  排序
     * @return mixed  获取所有
     * 获取所有
     */
    public function getAll($where = 'status=1', $relation = false, $order='id desc')
    {
        return $this->relation($relation)->where($where)->order($order)->select();
    }

    /**
     * @param $size
     * @param bool $relation
     * @param string $where
     * @param null $parse
     * @param string $order
     * @return array 分页获取
     * 分页获取
     */
    public function pageToView($size, $relation = false, $where = 'status=1', $parse = null, $order = 'id desc')
    {
        $count = $this->where($where, $parse)->count();
        $Page = new Page($count, $size);
        $Page->setConfig('prev', '上一页');
        $Page->setConfig('next', '下一页');
        $Page->setConfig('theme', '%HEADER% %FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END%');
        $show = $Page->show();
        $list = $this->where($where, $parse)->relation($relation)->order($order)->limit($Page->firstRow . ',' . $Page->listRows)->select();
        return array(
            'list' => $list,
            'page' => $show,
        );
    }

    /**
     * @param mixed $page
     * @param mixed|null $size
     * @param bool|false $relation
     * @param string $where
     * @param string $order
     * @return mixed 分页获取
     * 分页获取
     */
    public function listByPage($page = 1, $size = 10, $relation = false, $where = 'status=1', $order = 'id desc')
    {
        $list = $this->relation($relation)->where($where)->page($page, $size)->order($order)->select();
        return $list;
    }
}