/**
 * Created by Bensn on 16/5/31.
 */

'use strict';

var errTitle = '�쳣';
var warTitle = '����';
var errMsg = '�������쳣������ϵ����Ա';
var imgTitle = 'Ԥ��ͼƬ';

$(function () {
    formAjax($('form'));
    tableSort($('.table-sort'));
    tabCookies();
    coverClick();
    selectDefault();

    skinInput();
});

//selct��checkbox��hui��ʽ
function skinInput(){
    $('.skin-minimal input').iCheck({
        checkboxClass: 'icheckbox-blue',
        radioClass: 'iradio-blue',
        increaseArea: '20%'
    });
}

function selectDefault() {

}

/**
 * ��סҳ��tabλ��
 */
function tabCookies() {
    var tabBar = $('.tabBar span');
    tabBar.bind('click', function () {
        var index = $(this).index();
        var tabId = 'tab_' + window.location.href;
        $.cookie(tabId, index);
    })
}

/**
 * ��ȡtabλ��
 */
function getTabIndex() {
    var tabId = 'tab_' + window.location.href;
    return $.cookie(tabId) ? $.cookie(tabId) : 0;
}

/**
 * table����
 * @param obj
 */
function tableSort(obj) {
    var actionColumnIndex = $('thead th').length;
    if (!actionColumnIndex || actionColumnIndex < 1) {
        return;
    }
    obj.dataTable({
        "aaSorting": [[1, "desc"]],
        "bStateSave": false,
        "aoColumnDefs": [
            {
                "orderable": false, "aTargets": [0, actionColumnIndex - 1]
            }
        ],
        "aLengthMenu": [[20], [20]],
        "sPaginationType": "full_numbers"
    });

//    $('.table-sort').dataTable({
//        "aaSorting": [[ 1, "asc" ]],//Ĭ�ϵڼ�������
//        "bStateSave": false,//״̬����
//        "aoColumnDefs": [
//            {"orderable":false,"aTargets":[0]}// �ƶ��в���������
//        ],
//        "aLengthMenu": [[10,20], [10,20]],
//        "sPaginationType": "full_numbers",
//        "sDom": '<"top"fli>rt<"bottom">p<"clear">',
//    });
}

/**
 * ȫѡ��ѡ
 * @param obj
 * 1��ѡ
 * 2ȫѡ
 */
function checkAll(obj, type) {
    var checkboxs = obj.find('input[type="checkbox"]');
    if (type == 1) {
        checkboxs.each(function (i) {
            var item = checkboxs.eq(i);
            var isChek = item.is(':checked');
            item.prop('checked', !isChek);
        });
    }
    if (type == 2) {
        checkboxs.prop('checked', true);
    }
}

/**
 * ��һ���µ�tab
 * @param obj
 */
function showTab(obj) {
    Hui_admin_tab(obj);
}

/**
 * ��������
 * @param title
 * @param url
 * @param w
 * @param h
 */
function showIframe(title, url, w, h) {
    w = w ? w + "px" : '';
    h = h ? h + "px" : '';
    var index = layer.open({
        type: 2,
        title: title,
        content: url,
        area: [w, h],
        scrollbar: false
    });
    if ((!w || w == '') && (!h || h == '')) {
        layer.full(index);
    }
}

/**
 * ɾ����ʾ
 * @param url
 */
function delectOne(url) {
    layer.confirm('ȷ��Ҫɾ����', function () {
        requestUrl(url);
    });
}
/**
 * �ָ�ɾ����ʾ
 * @param url
 */
function recover(url) {
    layer.confirm('ȷ��Ҫ�ָ�ɾ����', function () {
        requestUrl(url);
    });
}

//��ʾ��ɾ������
function showdeleted(isdel){
    if(isdel){
        //alert(0);
        goUrl('?del=1');
    }else{
        goUrl('?del=0');
    }
}

/**
 * ɾ��ѡ��
 * @param url
 * @param itemObj
 */
function delectCheck(url, itemObj) {
    var size = $('.item:checked').size();
    if (size == 0){
        layer.msg('请选择要操作的项');
    }else {
        layer.confirm('ȷ��Ҫɾ��ѡ��������', function (index) {
            layer.close(index);
            var itemAll = itemObj;
            var ids = '';
            for (var i = 0; i < itemAll.size(); i++) {
                var item = itemAll.eq(i);
                var id = item.val();
                if (id > 0) {
                    if (!item.is(':checked')) {
                        continue;
                    }
                    ids += id + ',';
                }
            }
            requestUrl(url + '?id=' + ids);
        });
    }
}

/**
 * ���ѡ��
 * @param url
 * @param itemObj
 */
function reviewedCheck(url, itemObj) {
    layer.confirm('ȷ��Ҫ���ѡ��������', function (index) {
        layer.close(index);
        var itemAll = itemObj;
        var ids = '';
        for (var i = 0; i < itemAll.size(); i++) {
            var item = itemAll.eq(i);
            var id = item.val();
            if (id > 0) {
                if (!item.is(':checked')) {
                    continue;
                }
                ids += id + ',';
            }
        }
        requestUrl(url + '?id=' + ids);
    });
}

/**
 * ajax�ύ��
 * @param formObj
 */
function formAjax(formObj) {
    var count = formObj.length;
    for (var i = 0; i < count; i++) {
        var formItem = formObj.eq(i);
        formItem.Validform({
            tiptype: function (msg, o, cssctl) {
                if (o.type == 3) {
                    layer.msg(msg, function(){});
                }
            },
            callback: function (form) {
                var layars = layer.load(0, {shade: [0.8, '#fff']});
                formItem.ajaxSubmit({
                    url: formItem.attr('action'),
                    type: 'post',
                    enctype: 'multipart/form-data',
                    success: function (data) {
                        layer.close(layars);
                        runAjaxResSuccess(data);
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        layer.close(layars);
                        runAjaxResError(XMLHttpRequest, textStatus, errorThrown);
                    }
                });
                return false;
            }
        });
    }
}

/**
 * ��תurl
 * @param url
 * @param obj
 */
function goUrl(url, obj) {
    obj = obj ? obj : self;
    obj.location = url;
}


/**
 * ajax�ύ
 * */
function requestUrl(url) {
    var layars = layer.load(0, {shade: [0.8, '#fff']});
    $.ajax({
        url: url,
        success: function (data) {
            layer.close(layars);
            runAjaxResSuccess(data);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            layer.close(layars);
            runAjaxResError(XMLHttpRequest, textStatus, errorThrown);
        }
    });
}

/**
 * ����ajax����ɹ�ʱ���ص�����
 * @param data
 */
function runAjaxResSuccess(data) {
    if (data.status === 1) {
        layer.msg(data.info, {
            icon: 1,
            time: 1000,
            shade: [0.8, '#fff']
        }, function (index) {
            //���ҳ�����ڵ���
            var indexbox = parent.layer.getFrameIndex(window.name);
            if (indexbox) {
                //�رյ���
                if (data.url) {
                    //goUrl(data.url, data.url.indexOf("#this") ? this : parent);
                    if (data.url.indexOf("#this") > 0) {
                        goUrl(data.url, window);
                    } else {
                        goUrl(data.url, parent);
                    }
                    return;
                } else {
                    parent.location.reload();
                }
                parent.layer.close(indexbox);
            }
            layer.close(index);
            if (data.url) {
                goUrl(data.url);
            }
        });
    } else if (data.status === 0) {
        layer.alert(data.info, {
            title: warTitle,
            icon: 2
        }, function (index) {
            //���ҳ�����ڵ���
            var indexbox = parent.layer.getFrameIndex(window.name);
            if (indexbox) {
                //�رյ���
                if (data.url) {
                    goUrl(data.url, parent);
                }
                parent.layer.close(indexbox);
            }
            layer.close(index);
            if (data.url) {
                goUrl(data.url);
            }
        });
    } else {
        layer.alert(errMsg, {
            icon: 0,
            title: errTitle
        });
    }
}


/**
 * ����ajax����ʧ��ʱ���ص�����
 * @param XMLHttpRequest
 * @param textStatus
 * @param errorThrown
 */
function runAjaxResError(XMLHttpRequest, textStatus, errorThrown) {
    var errorInfo = $(XMLHttpRequest.responseText).find('h1').text();
    layer.alert(errorInfo + '<br/>' + errMsg, {
        icon: 0,
        title: errTitle
    });
}

/**
 * ����ͼ���
 */
function coverClick() {
    $('.cover').on('click', function () {
        previewPictures($(this).attr('src'));
    });
}

/**
 * Ԥ��ͼƬ
 * @param url
 */
function previewPictures(url) {
    layer.photos({
        photos: {
            "title": imgTitle, //������
            "id": 1, //���id
            "start": 0, //��ʼ��ʾ��ͼƬ��ţ�Ĭ��0
            "data": [   //��������ͼƬ�������ʽ
                {
                    "alt": imgTitle,
                    "pid": 1, //ͼƬid
                    "src": url, //ԭͼ��ַ
                    "thumb": "" //����ͼ��ַ
                }
            ]
        }
    });
}

/**
 * Ԥ���б�
 * @param id
 */
function previewPicturesList(id) {
    layer.photos({
        photos: id
    });
}

/**
 * �л���ʾ������
 * @param jqobj
 */
function toggleShow(jqobj) {
    var list = jqobj.next();
    list.slideToggle('slow', function () {
        jqobj.html(list.css('display') == 'none' ? '�����ʾ' : '�������');
    });
}