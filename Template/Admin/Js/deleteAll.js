/**
 * Created by change on 2017/1/23.
 */
/**
 * 删除选中
 * @param url
 * @param itemObj
 */
function delectCheck(url, itemObj) {
    var size = $('.item:checked').size();
    if (size == 0){
        layer.msg('请选择要操作的项');
    }else{
        layer.confirm('确认要删除选中数据吗？', function (index) {
            layer.close(index);
            var itemAll = itemObj;
            var ids = '';
            for (var i = 0; i < itemAll.size(); i++) {
                var item = itemAll.eq(i);
                var id = item.val();
                if (id > 0) {
                    if (!item.is(':checked')) {
                        continue;
                    }
                    ids += id + ',';
                }
            }
            requestUrl(url + '?id=' + ids);
        });
    }
}
/**
 * ajax提交表单
 * @param formObj
 */
// function formAjax(formObj) {
//     var count = formObj.length;
//     for (var i = 0; i < count; i++) {
//         var formItem = formObj.eq(i);
//         formItem.Validform({
//             tiptype: function (msg, o, cssctl) {
//                 if (o.type == 3) {
//                     layer.msg(msg, function(){});
//                 }
//             },
//             callback: function (form) {
//                 var layars = layer.load(0, {shade: [0.8, '#fff']});
//                 formItem.ajaxSubmit({
//                     url: formItem.attr('action'),
//                     type: 'post',
//                     enctype: 'multipart/form-data',
//                     success: function (data) {
//                         layer.close(layars);
//                         runAjaxResSuccess(data);
//                     },
//                     error: function (XMLHttpRequest, textStatus, errorThrown) {
//                         alert(321);
//                         layer.close(layars);
//                         runAjaxResError(XMLHttpRequest, textStatus, errorThrown);
//                     }
//                 });
//                 return false;
//             }
//         });
//     }
// }