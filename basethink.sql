/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50553
Source Host           : localhost:3306
Source Database       : basethink

Target Server Type    : MYSQL
Target Server Version : 50553
File Encoding         : 65001

Date: 2017-04-11 14:58:55
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for b_admin_user
-- ----------------------------
DROP TABLE IF EXISTS `b_admin_user`;
CREATE TABLE `b_admin_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `pwd` varchar(255) DEFAULT NULL,
  `cover` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `phone` bigint(11) DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL,
  `user_group_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `create_time` bigint(20) DEFAULT NULL,
  `last_time` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of b_admin_user
-- ----------------------------
INSERT INTO `b_admin_user` VALUES ('1', 'admin', 'e10adc3949ba59abbe56e057f20f883e', null, '', '0', 'b1a1da8526ca33dc3226c16ecd8a239a', '1', '1', '1487816471', '1487816471');

-- ----------------------------
-- Table structure for b_column
-- ----------------------------
DROP TABLE IF EXISTS `b_column`;
CREATE TABLE `b_column` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `column_id` int(11) DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  `model_id` int(11) DEFAULT NULL,
  `cover` varchar(255) DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL,
  `status` int(255) DEFAULT NULL,
  `create_time` int(11) DEFAULT NULL,
  `last_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of b_column
-- ----------------------------

-- ----------------------------
-- Table structure for b_config
-- ----------------------------
DROP TABLE IF EXISTS `b_config`;
CREATE TABLE `b_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(255) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of b_config
-- ----------------------------
INSERT INTO `b_config` VALUES ('1', 'site_name', '小视频');

-- ----------------------------
-- Table structure for b_menu
-- ----------------------------
DROP TABLE IF EXISTS `b_menu`;
CREATE TABLE `b_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL,
  `menu_id` int(11) DEFAULT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `create_time` int(11) DEFAULT NULL,
  `last_time` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=48 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of b_menu
-- ----------------------------
INSERT INTO `b_menu` VALUES ('1', '用户管理', '用户信息管理', '0', '&#xe60d;', '', '', '0', '1487765538', '1490337049', '1');
INSERT INTO `b_menu` VALUES ('2', '用户管理', '用户信息管理', '1', '', 'User/showList', '', '0', '1487765919', '1487767496', '1');
INSERT INTO `b_menu` VALUES ('3', '系统配置', '系统信息配置', '0', '&#xe61d;', '', '', '0', '1487768114', '1487768647', '1');
INSERT INTO `b_menu` VALUES ('4', '站点设置', '站点信息设置', '3', '', 'Config/siteInfo', '', '0', '1487768593', '1487768638', '1');
INSERT INTO `b_menu` VALUES ('5', '菜单管理', '菜单信息管理', '3', '', 'Menu/showList', '', '0', '1487768620', '1487768620', '1');
INSERT INTO `b_menu` VALUES ('6', '后台用户', '后台用户信息管理', '3', '', 'AdminUser/showList', '', '0', '1487768734', '1487768743', '1');
INSERT INTO `b_menu` VALUES ('7', '用户角色', '后台用户角色信息管理', '3', '', 'UserGroup/showList', '', '0', '1487768778', '1487768778', '1');

-- ----------------------------
-- Table structure for b_model
-- ----------------------------
DROP TABLE IF EXISTS `b_model`;
CREATE TABLE `b_model` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `content` longtext,
  `create_time` bigint(20) DEFAULT NULL,
  `last_time` bigint(20) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of b_model
-- ----------------------------
INSERT INTO `b_model` VALUES ('1', '栏目分类', null, null, null, '1');
INSERT INTO `b_model` VALUES ('2', '图文信息', null, null, null, '1');
INSERT INTO `b_model` VALUES ('3', '单篇文章', null, null, null, '1');
INSERT INTO `b_model` VALUES ('4', '图库专辑', null, null, null, '1');
INSERT INTO `b_model` VALUES ('5', '视频', null, null, null, '1');

-- ----------------------------
-- Table structure for b_user
-- ----------------------------
DROP TABLE IF EXISTS `b_user`;
CREATE TABLE `b_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `level_id` int(11) DEFAULT '1',
  `name` varchar(255) DEFAULT NULL,
  `pwd` varchar(255) DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL,
  `cover` varchar(255) DEFAULT NULL,
  `gender` int(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `nick` varchar(255) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `true_name` varchar(255) DEFAULT NULL,
  `integral` bigint(20) DEFAULT NULL,
  `describe` varchar(255) DEFAULT NULL,
  `status` int(255) DEFAULT NULL,
  `create_time` int(11) DEFAULT NULL,
  `last_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of b_user
-- ----------------------------

-- ----------------------------
-- Table structure for b_user_group
-- ----------------------------
DROP TABLE IF EXISTS `b_user_group`;
CREATE TABLE `b_user_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `title` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `create_time` int(11) DEFAULT NULL COMMENT '创建时间',
  `last_time` int(11) DEFAULT NULL COMMENT '更新时间',
  `menu` longtext,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of b_user_group
-- ----------------------------
INSERT INTO `b_user_group` VALUES ('1', '系统管理员', '1', '1482413589', '1491890548', '[\"1\",\"2\",\"3\",\"4\",\"5\",\"6\",\"7\"]');
INSERT INTO `b_user_group` VALUES ('2', '普通用户', '1', '1482413589', '1489375987', null);
INSERT INTO `b_user_group` VALUES ('3', '高级用户', '0', '1484042257', '1484042257', '[\"19\",\"20\",\"12\",\"18\",\"13\",\"14\",\"15\",\"22\",\"23\",\"24\"]');
INSERT INTO `b_user_group` VALUES ('4', '高级会员', '0', '1484120410', '1484120654', '[\"12\"]');
